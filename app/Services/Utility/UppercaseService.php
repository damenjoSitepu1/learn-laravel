<?php

namespace App\Services\Utility;

class UppercaseService
{
    private $stringOfWord;

    /**
     * Fill all the 
     */
    public function __construct(string $stringOfWord)
    {
        $this->stringOfWord = $stringOfWord;
    }

    /**
     * Uppercase the words base on user parameter
     */
    public function __invoke()
    {
        return strtoupper($this->stringOfWord);
    }
}
