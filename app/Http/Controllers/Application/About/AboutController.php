<?php

namespace App\Http\Controllers\Application\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function __invoke()
    {
        echo "About Page Invoked";
    }
}
