<?php

namespace App\Http\Controllers\Application\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

// Services
use App\Services\Utility\UppercaseService as Uppercase;

class HomeController extends Controller
{
    private $myWord = 'Damenjo Sitepu';
    private $uppercaseService;

    // Instantiate New Services
    public function __construct()
    {
        $this->uppercaseService = new Uppercase($this->myWord);
    }

    public function index()
    {
        $var = new Uppercase('Tari puspita sari');
        dd($var());
    }
}
