<?php

use App\Http\Controllers\Application\About\AboutController;
use App\Http\Controllers\Application\Home\HomeController;
use App\Http\Controllers\Application\Setting\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Home section
Route::get('', [HomeController::class, 'index'])->name('home.index');
// About section
Route::get('about', AboutController::class)->name('about.index');
// Setting Section 
// Route::resource('setting', SettingController::class);
Route::prefix('setting')->group(function () {
    Route::controller(SettingController::class)->group(function () {
        Route::get('', 'index')->name('setting.index');
        Route::get('profile', 'profile')->name('setting.profile');
        Route::get('security', 'security')->name('setting.security');
        Route::get('personal-access-token', 'personalAccessToken')->name('setting.personal-access-token');
        Route::get('news', 'news')->name('setting.news');
    });

    // Route::get('', [SettingController::class, 'index'])->name('setting.index');
    // Route::get('profile', [SettingController::class, 'profile'])->name('setting.profile');
    // Route::get('security', [SettingController::class, 'security'])->name('setting.security');
    // Route::get('personal-access-token', [SettingController::class, 'personalAccessToken'])->name('setting.personal-access-token');
    // Route::get('news', [SettingController::class, 'news'])->name('setting.news');
});
