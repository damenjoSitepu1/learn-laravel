<?php

namespace App\Http\Controllers\Application\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        echo 'Setting Page - Index Page';
    }

    public function profile()
    {
        echo "Setting Page - Profile Section";
    }

    public function security()
    {
        echo "Setting Page - Security Page";
    }

    public function personalAccessToken()
    {
        echo "Setting Page - PAT Page";
    }

    public function news()
    {
        echo "Setting Page - News Page";
    }
}
